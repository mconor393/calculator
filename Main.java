import java.util.Scanner;

public class Calculator {
    private final Scanner scanner = new Scanner(System.in);
    private final int systemBase;

    public Calculator(int systemBase) {
        this.systemBase = systemBase;
    }

    public void run() {
        System.out.println("*--------------КАЛЬКУЛЯТОР---------------*");
        int num1 = readNumber("первое");
        int num2 = readNumber("второе");
        String operation = readOperation();
        int result = calculate(num1, num2, operation);
        printAnswerInAllBases(result);
    }

    private int readNumber(String name) {
        System.out.printf("Введите %s число: ", name);
        String input = scanner.next();
        try {
            return Integer.parseInt(input, systemBase);
        } catch (NumberFormatException e) {
            System.out.println("Неправильный формат числа");
            return readNumber(name);
        }
    }

    private String readOperation() {
        System.out.print("Введите операцию (+, -, *, /, ^): ");
        String operation = scanner.next();
        if (!operation.matches("[+\\-*/^]")) {
            System.out.println("Неправильный символ операции");
            return readOperation();
        }
        return operation;
    }

    private int calculate(int num1, int num2, String operation) {
        switch (operation) {
            case "+" -> {
                return num1 + num2;
            }
            case "-" -> {
                return num1 - num2;
            }
            case "*" -> {
                return num1 * num2;
            }
            case "/" -> {
                if (num2 == 0) {
                    System.out.println("Ошибка: деление на ноль!");
                    return calculate(num1, readNumber("Новое второе число: "), "/");
                }
                return num1 / num2;
            }
            case "^" -> {
                return (int) Math.pow(num1, num2);
            }
            default -> {
                System.out.println("Неизвестная операция");
                return 0;
            }
        }
    }

    private void printAnswerInAllBases(int answer) {
        String binaryAnswer = Integer.toBinaryString(answer);
        String octalAnswer = Integer.toOctalString(answer);
        String decimalAnswer = Integer.toString(answer);
        String hexAnswer = Integer.toHexString(answer);
        System.out.println("Ответ:");
        System.out.printf("%10s: %s%n", "Двоичная", binaryAnswer);
        System.out.printf("%10s: %s%n", "Восьмеричная", octalAnswer);
        System.out.printf("%10s: %s%n", "Десятичная", decimalAnswer);
        System.out.printf("%10s: %s%n", "Шестнадцатеричная", hexAnswer);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите систему счисления (2, 8, 10, 16): ");
        int systemBase = scanner.nextInt();
        Calculator calculator = new Calculator(systemBase);
        calculator.run();
    }
}